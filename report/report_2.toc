\babel@toc {english}{}
\contentsline {section}{\numberline {1}T\IeC {\'o}m T\IeC {\'\abreve }t}{3}
\contentsline {section}{\numberline {2}C\IeC {\ohorn } s\IeC {\h \ohorn } l\IeC {\'y} thuy\IeC {\'\ecircumflex }t}{3}
\contentsline {subsection}{\numberline {2.1}Gi\IeC {\'\ohorn }i thi\IeC {\d \ecircumflex }u v\IeC {\`\ecircumflex } SQL injection}{3}
\contentsline {subsection}{\numberline {2.2}C\IeC {\'a}c l\IeC {\~\ocircumflex }i th\IeC {\uhorn }\IeC {\`\ohorn }ng g\IeC {\d \abreve }p}{6}
\contentsline {subsubsection}{\numberline {2.2.1}Kh\IeC {\^o}ng ki\IeC {\h \ecircumflex }m tra k\IeC {\'y} t\IeC {\d \uhorn } tho\IeC {\'a}t truy v\IeC {\'\acircumflex }n}{6}
\contentsline {subsubsection}{\numberline {2.2.2}X\IeC {\h \uhorn } l\IeC {\'y} kh\IeC {\^o}ng \IeC {\dj }\IeC {\'u}ng ki\IeC {\h \ecircumflex }u}{7}
\contentsline {subsubsection}{\numberline {2.2.3}Blind SQL injection}{8}
\contentsline {subsection}{\numberline {2.3}M\IeC {\d \ocircumflex }t s\IeC {\'\ocircumflex } d\IeC {\d a}ng t\IeC {\'\acircumflex }n c\IeC {\^o}ng b\IeC {\`\abreve }ng SQL injection th\IeC {\uhorn }\IeC {\`\ohorn }ng g\IeC {\d \abreve }p}{8}
\contentsline {subsubsection}{\numberline {2.3.1}V\IeC {\uhorn }\IeC {\d \ohorn }t qua \IeC {\dj }\IeC {\u a}ng nh\IeC {\d \acircumflex }p}{8}
\contentsline {subsubsection}{\numberline {2.3.2}S\IeC {\h \uhorn } d\IeC {\d u}ng c\IeC {\^a}u l\IeC {\d \ecircumflex }nh SELECT}{9}
\contentsline {subsubsection}{\numberline {2.3.3}s\IeC {\h \uhorn } d\IeC {\d u}ng c\IeC {\^a}u l\IeC {\d \ecircumflex }nh INSERT}{10}
\contentsline {subsubsection}{\numberline {2.3.4}S\IeC {\h \uhorn } d\IeC {\d u}ng c\IeC {\'a}c stored-procedures}{11}
\contentsline {subsubsection}{\numberline {2.3.5}C\IeC {\'a}c b\IeC {\uhorn }\IeC {\'\ohorn }c c\IeC {\ohorn } b\IeC {\h a}n \IeC {\dj }\IeC {\h \ecircumflex } t\IeC {\'\acircumflex }n c\IeC {\^o}ng b\IeC {\`\abreve }ng SQL inject}{11}
\contentsline {subsection}{\numberline {2.4}C\IeC {\'a}ch ph\IeC {\`o}ng ch\IeC {\'\ocircumflex }ng}{11}
\contentsline {subsection}{\numberline {2.5}Gi\IeC {\'\ohorn }i thi\IeC {\d \ecircumflex }u v\IeC {\`\ecircumflex } sqlmap}{12}
\contentsline {subsubsection}{\numberline {2.5.1}Gi\IeC {\'\ohorn }i thi\IeC {\d \ecircumflex }u}{12}
\contentsline {subsubsection}{\numberline {2.5.2}T\IeC {\'\i }nh n\IeC {\u a}ng}{13}
\contentsline {section}{\numberline {3}Demo c\IeC {\ohorn } b\IeC {\h a}n v\IeC {\`\ecircumflex } l\IeC {\~\ocircumflex }i SQL Injection}{14}
\contentsline {subsection}{\numberline {3.1}T\IeC {\`\i }m m\IeC {\d \ocircumflex }t trang web c\IeC {\'o} th\IeC {\h \ecircumflex } b\IeC {\d i} t\IeC {\'\acircumflex }n c\IeC {\^o}ng b\IeC {\h \ohorn }i k\IeC {\~y} thu\IeC {\d \acircumflex }t n\IeC {\`a}y.}{14}
\contentsline {subsection}{\numberline {3.2}Xu\IeC {\'\acircumflex }t c\IeC {\'a}c database c\IeC {\'o} trong trang web.}{17}
\contentsline {subsection}{\numberline {3.3}Xu\IeC {\'\acircumflex }t t\IeC {\^e}n t\IeC {\'\acircumflex }t c\IeC {\h a} c\IeC {\'a}c b\IeC {\h a}ng c\IeC {\'o} trong database}{18}
\contentsline {subsection}{\numberline {3.4}Xu\IeC {\'\acircumflex }t t\IeC {\'\acircumflex }t c\IeC {\h a} c\IeC {\'a}c c\IeC {\d \ocircumflex }t c\IeC {\'o} trong b\IeC {\h a}ng.}{18}
\contentsline {subsection}{\numberline {3.5}Xu\IeC {\'\acircumflex }t c\IeC {\'a}c tr\IeC {\uhorn }\IeC {\`\ohorn }ng d\IeC {\~\uhorn } li\IeC {\d \ecircumflex }u trong b\IeC {\h a}ng ra.}{19}
\contentsline {section}{\numberline {4}Ph\IeC {\^a}n t\IeC {\'\i }ch v\IeC {\`a} k\IeC {\'\ecircumflex }t lu\IeC {\d \acircumflex }n}{20}
\contentsline {section}{\numberline {5}H\IeC {\uhorn }\IeC {\'\ohorn }ng ph\IeC {\'a}t tri\IeC {\h \ecircumflex }n}{20}
\contentsline {section}{\numberline {6}Tham kh\IeC {\h a}o}{20}
\contentsline {section}{\numberline {7}ph\IeC {\d u} l\IeC {\d u}c 1:}{20}
